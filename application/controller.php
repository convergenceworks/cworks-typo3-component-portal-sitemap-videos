<?php

class ComponentController_sitemap_videos {

	function render(){

		$data = array();

        $data['videos'] = $this->providerData['externalVideos'];
        $data['pagebrowse'] = $this->pagebrowse;
        $data['term'] = $this->helper->getQueryStringParam("externalVideos","searchTerm");
        $data['showIndex'] = 1;

        $page = (int)$this->helper->getQueryStringParam('externalVideos','page');

        if($page || $this->pagebrowse["total"] < $this->componentData['config']['itemCount']) {
            $data['showIndex'] = 0;
        }

        return array("data" => $data);	
    }
}
?>
